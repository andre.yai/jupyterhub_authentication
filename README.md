# JUPYTERHUB AUTHENTICATOR

Neste projeto tem uma imagem docker para roda la jupyterhub -f /etc/jupyterhub/jupyterhub_config.py
- Basicamente ela veio de um conteiner docker do jupyterhub. Link util como iniciar : https://www.youtube.com/watch?v=NuzYSNTBvZU&list=LL&index=4&t=10s&ab_channel=HatariLabs e https://www.hatarilabs.com/ih-en/how-to-set-a-multiuser-jupyterlab-server-with-jupyterhub-in-windows-with-docker
- Em seguida testamos diferentes authenticadores e spawners. O que deu mais certo foi o tmpauthenticator.
- Alem disto foi adicionado algumas bibliotecas adicionais como:  pandas.
- Arquivos modificados o arquivo jupyter_config,py, tmpauthenticator/__init__.py (onde pega o token do usuário e tenta logar) e handlers/base.py (onde le a uri e salva o token num arquivo .csv - dentro da pasta do tmpauthenticator.)

Para testar então:
- Configurar as postas para rodar. 
- rodar o comando jupyterhub -f /etc/jupyterhub/jupyterhub_config.py
- Criar o usuário que irá testar o login
- dai acessar <host>:<porta./?token=<token>

